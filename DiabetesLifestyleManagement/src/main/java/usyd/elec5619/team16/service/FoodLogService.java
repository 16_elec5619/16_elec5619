package usyd.elec5619.team16.service;

import java.util.List;

import usyd.elec5619.team16.model.FoodLog;

public interface FoodLogService {
    public void addFoodLog(FoodLog foodLog); //add a food intakes log.
    public void deleteFoodLog(int id);   // delete a food log indexed by id
	public void updateFoodLog(FoodLog foodLog); //up date food log
    public List<FoodLog> listFoodLog();  // get all food log in the database
}
