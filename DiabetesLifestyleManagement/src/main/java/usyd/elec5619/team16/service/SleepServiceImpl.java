package usyd.elec5619.team16.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fitbit.api.common.model.timeseries.Data;
import com.fitbit.api.common.model.timeseries.TimePeriod;
import com.fitbit.api.common.model.timeseries.TimeSeriesResourceType;

import usyd.elec5619.team16.model.SleepGoal;
import usyd.elec5619.team16.model.SleepLog;
import usyd.elec5619.team16.model.User;

@Service
@Transactional
public class SleepServiceImpl implements SleepService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
    @Autowired
	private FitbitService fitbitService;
	
	//SleepLog
	@Override
	public void addSleepLog(SleepLog SleepLog) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(SleepLog);
	}

	@Override
	public void deleteSleepLog(int id) {
		// TODO Auto-generated method stub
		
		SleepLog log =(SleepLog) this.sessionFactory.getCurrentSession().get(SleepLog.class,id);
		
		this.sessionFactory.getCurrentSession().delete(log);
	}

	@Override
	public void updateSleepLog(SleepLog sleepLog) {
		this.sessionFactory.getCurrentSession().merge(sleepLog);
	}

	@Override
	public List<SleepLog> listSleepLog() {
		// TODO Auto-generated method stub
		return null;
	}

	
	//SleepGoals
	@SuppressWarnings("unchecked")
	@Override
	public List<SleepGoal> getSleepGoals(User user) {
		// TODO Auto-generated method stub
		String Query = "FROM SleepGoal  ";
		
		return this.sessionFactory.getCurrentSession().createQuery(Query).list();
	}

	@Override
	public void addSleepGoal(SleepGoal sleepGoal) {
		this.sessionFactory.getCurrentSession().save(sleepGoal);
	}

	@Override
	public void deleteSleepgoal(int id) {
		SleepGoal deleteitem = (SleepGoal) this.sessionFactory.getCurrentSession().get(SleepGoal.class, id);
		this.sessionFactory.getCurrentSession().delete(deleteitem);
	}

	@Override
	public List<SleepLog> getSleepLogByTime(HttpServletRequest request,
			HttpServletResponse response, TimePeriod period,User currentUser) {
		//get past activity info
		List<Data> minutesAsleeps = fitbitService.getActivityInfo(request, response, TimeSeriesResourceType.MINUTES_ASLEEP, period);
		List<Data> efficiencys = fitbitService.getActivityInfo(request, response, TimeSeriesResourceType.EFFICIENCY, period);
		List<Data> minutesToFallAsleeps = fitbitService.getActivityInfo(request, response, TimeSeriesResourceType.MINUTES_TO_FALL_ASLEEP, period);
		
		List<SleepLog> logs = new ArrayList<SleepLog>();
		for (int i = 0; i <minutesAsleeps.size(); i++){
			SleepLog log = new SleepLog();
			log.setMinutesAsleep(minutesAsleeps.get(i).intValue());
			log.setEfficiency(efficiencys.get(i).intValue());
			log.setMinutesToFallAsleep(minutesToFallAsleeps.get(i).intValue());
			log.setUser(currentUser);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date date = sdf.parse(minutesAsleeps.get(i).getDateTime());
				log.setDate(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logs.add(log);
			
			addSleepLog(log);
		}
			
		return logs;
	}
	
	

}
