package usyd.elec5619.team16.service;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import usyd.elec5619.team16.model.SleepLog;

import com.fitbit.api.FitbitAPIException;
import com.fitbit.api.client.FitbitAPIEntityCache;
import com.fitbit.api.client.FitbitApiClientAgent;
import com.fitbit.api.client.FitbitApiCredentialsCache;
import com.fitbit.api.client.FitbitApiSubscriptionStorage;
import com.fitbit.api.client.LocalUserDetail;
import com.fitbit.api.client.service.FitbitAPIClientService;
import com.fitbit.api.common.model.activities.Activities;
import com.fitbit.api.common.model.activities.ActivityCategory;
import com.fitbit.api.common.model.foods.Food;
import com.fitbit.api.common.model.sleep.Sleep;
import com.fitbit.api.common.model.timeseries.Data;
import com.fitbit.api.common.model.timeseries.TimePeriod;
import com.fitbit.api.common.model.timeseries.TimeSeriesResourceType;
import com.fitbit.api.common.model.user.UserInfo;
import com.fitbit.api.model.APIResourceCredentials;
import com.fitbit.api.model.ApiSubscription;
import com.fitbit.api.model.FitbitUser;

@Service
public class FitbitServiceImp implements FitbitService,InitializingBean{
    protected Log log = LogFactory.getLog(getClass());

    private static final int APP_USER_COOKIE_TTL = 60 * 60 * 24 * 7 * 4;
    private static final String APP_USER_COOKIE_NAME = "exampleClientUid";

    public static final String OAUTH_TOKEN = "oauth_token";
    public static final String OAUTH_VERIFIER = "oauth_verifier";

    @Resource
    private FitbitAPIEntityCache entityCache;
    @Resource
    private FitbitApiCredentialsCache credentialsCache;
    @Resource
    private FitbitApiSubscriptionStorage subscriptionStore;

    @Value("#{config['fitbitSiteBaseUrl']}")
    private String fitbitSiteBaseUrl;
    @Value("#{config['apiBaseUrl']}")
    private String apiBaseUrl;
    @Value("#{config['exampleBaseUrl']}")
    private String exampleBaseUrl;

    @Value("#{config['clientConsumerKey']}")
    private String clientConsumerKey;
    @Value("#{config['clientSecret']}")
    private String clientSecret;
    
    @Override
    public void afterPropertiesSet() throws Exception {
        apiClientService = new FitbitAPIClientService<FitbitApiClientAgent>(
                new FitbitApiClientAgent(getApiBaseUrl(), getFitbitSiteBaseUrl(), credentialsCache),
                clientConsumerKey,
                clientSecret,
                credentialsCache,
                entityCache,
                subscriptionStore
        );
    }
    
	@Override
	public String authorize(HttpServletRequest request, HttpServletResponse response, String completeRequest) {
		// TODO Auto-generated method stub
	    RequestContext context = new RequestContext();
	    populate(context, request, response);
	    
        context.getApiClientService().expireResourceCredentials(context.getOurUser());
	    try {        
	    	// Redirect to page where user can authorize the application:
	        return "redirect:" + context.getApiClientService().getResourceOwnerAuthorizationURL(context.getOurUser(), getExampleBaseUrl() + completeRequest);
	    } catch (FitbitAPIException e) {
	        log.error(e);
	        request.setAttribute("errors", Collections.singletonList(e.getMessage()));
	        return null;
	    }	
	}
	
    public void completeAuthorization(HttpServletRequest request, HttpServletResponse response) {
        RequestContext context = new RequestContext();
        populate(context, request, response);
        String tempTokenReceived = request.getParameter(OAUTH_TOKEN);
        String tempTokenVerifier = request.getParameter(OAUTH_VERIFIER);
        APIResourceCredentials resourceCredentials = context.getApiClientService().getResourceCredentialsByTempToken(tempTokenReceived);

        if (resourceCredentials == null) {
            log.error("Unrecognized temporary token when attempting to complete authorization: " + tempTokenReceived);
            request.setAttribute("errors", "Unrecognized temporary token when attempting to complete authorization: " + tempTokenReceived);
        } else {
            // Get token credentials only if necessary:
            if (!resourceCredentials.isAuthorized()) {
                // The verifier is required in the request to get token credentials:
                resourceCredentials.setTempTokenVerifier(tempTokenVerifier);
                try {
                    // Get token credentials for user:
                    context.getApiClientService().getTokenCredentials(new LocalUserDetail(resourceCredentials.getLocalUserId()));
                } catch (FitbitAPIException e) {
                    log.error("Unable to finish authorization with Fitbit.", e);
                    request.setAttribute("errors", Collections.singletonList(e.getMessage()));
                }
            }
        }
    }
    
	@Override
	public List<Food> searchFoods(HttpServletRequest request, HttpServletResponse response,String searchName) {
		// TODO Auto-generated method stub
		
        RequestContext context = new RequestContext();
        populate(context, request, response);
        if (!isAuthorized(context, request)) {
            return null;
        }

        try {
        	return context.getApiClientService().getClient().searchFoods(context.getOurUser(), searchName);
        } catch (FitbitAPIException e) {
            log.error("Error during getting sleep.", e);
        }
        return null;
	}
	
	public List<Data> getActivityInfo(HttpServletRequest request, HttpServletResponse response, TimeSeriesResourceType resourceType, TimePeriod period) {
		RequestContext context = new RequestContext();
        populate(context, request, response);
        
        FitbitUser user = FitbitUser.CURRENT_AUTHORIZED_USER;
        
        try {
        	return context.getApiClientService().getClient().getTimeSeries(context.getOurUser(), user, resourceType, "today", period);
        } catch (FitbitAPIException e) {
            log.error("Error during getting activity info.", e);
        }
        return null;
	}
	
	public UserInfo getUserInfo(HttpServletRequest request, HttpServletResponse response)
	{
		RequestContext context = new RequestContext();
        populate(context, request, response);
        
        //FitbitUser user = FitbitUser.CURRENT_AUTHORIZED_USER;
        //LocalUserDetail userInfo = new LocalUserDetail(apiBaseUrl);
        try
        {
        	return context.getApiClientService().getClient().getUserInfo(context.ourUser);
        }
        catch(FitbitAPIException e)
        {
        	log.error("Error during getting User info.", e);
        }
        return null;
	}
	@Override
	public Activities getActivityGoals(HttpServletRequest request, HttpServletResponse response) 
	{
		RequestContext context = new RequestContext();
        populate(context, request, response);
        
        FitbitUser user = FitbitUser.CURRENT_AUTHORIZED_USER;
        
        try {
        	return context.getApiClientService().getClient().getActivities(context.ourUser, user, LocalDate.now());
        } catch (FitbitAPIException e) {
            log.error("Error during getting activity goals.", e);
        }
        return null;
	}
	
	public Sleep getSleep(HttpServletRequest request, HttpServletResponse response, LocalDate  date) {
		RequestContext context = new RequestContext();
        populate(context, request, response);
        
        FitbitUser user = FitbitUser.CURRENT_AUTHORIZED_USER;
        
        try {
        	return context.getApiClientService().getClient().getSleep(context.getOurUser(),user,date);
        } catch (FitbitAPIException e) {
            log.error("Error during getting sleep.", e);
        }
        return null;
	}

    protected boolean isAuthorized(RequestContext context, HttpServletRequest request) {
        // Get cached resource credentials:
        APIResourceCredentials resourceCredentials = context.getApiClientService().getResourceCredentialsByUser(context.getOurUser());
        boolean isAuthorized = resourceCredentials != null && resourceCredentials.isAuthorized();
        request.setAttribute("isAuthorized", isAuthorized);
        if (resourceCredentials != null) {
            request.setAttribute("encodedUserId", resourceCredentials.getResourceId());
            request.setAttribute("userProfileURL", getFitbitSiteBaseUrl() + "/user/" + resourceCredentials.getResourceId());
        }
        return isAuthorized;
    }

	public void populate(RequestContext context, HttpServletRequest request, HttpServletResponse response) {
	        context.setApiClientService(apiClientService);
	        context.setOurUser(getOrMakeExampleAppUser(request, response));
	        APIResourceCredentials resourceCredentials = context.getApiClientService().getResourceCredentialsByUser(context.getOurUser());
	        boolean isAuthorized = resourceCredentials != null && resourceCredentials.isAuthorized();
	        boolean isSubscribed = false;
	        if (isAuthorized) {
	            List<ApiSubscription> subscriptions = Collections.emptyList();
	            try {
	                subscriptions = apiClientService.getClient().getSubscriptions(context.getOurUser());
	            } catch (FitbitAPIException e) {
	                log.error("Subscription error: " + e, e);
	            }
	            if (null != context.getOurUser() && subscriptions.size() > 0) {
	                isSubscribed = true;
	            }
	        }
	        request.setAttribute("actionBean", context);
	        request.setAttribute("isSubscribed", isSubscribed);
	        request.setAttribute("exampleBaseUrl", getExampleBaseUrl());
	}
	protected LocalUserDetail getOrMakeExampleAppUser(HttpServletRequest request, HttpServletResponse response) {
	        String userId = null;

	        Cookie[] cookies = request.getCookies();
	        if (null != cookies) {
	            for (Cookie cookie : cookies) {
	                if (cookie.getName().equals(APP_USER_COOKIE_NAME)) {
	                    userId = cookie.getValue();
	                    log.info("Returning user " + userId);
	                    break;
	                }
	            }
	        }
	        if (null == userId || userId.length() < 1) {
	            userId = String.valueOf(Math.abs(new Random(System.currentTimeMillis()).nextInt()));
	            Cookie uidCookie = new Cookie(APP_USER_COOKIE_NAME, userId);
	            uidCookie.setPath("/");
	            uidCookie.setMaxAge(APP_USER_COOKIE_TTL);
	            response.addCookie(uidCookie);
	            log.info("Created new user " + userId);
	        }

	        return new LocalUserDetail(userId);
	}
    public FitbitAPIEntityCache getEntityCache() {
		return entityCache;
	}
	public void setEntityCache(FitbitAPIEntityCache entityCache) {
		this.entityCache = entityCache;
	}
	public FitbitApiCredentialsCache getCredentialsCache() {
		return credentialsCache;
	}

	public void setCredentialsCache(FitbitApiCredentialsCache credentialsCache) {
		this.credentialsCache = credentialsCache;
	}

	public String getFitbitSiteBaseUrl() {
		return fitbitSiteBaseUrl;
	}

	public void setFitbitSiteBaseUrl(String fitbitSiteBaseUrl) {
		this.fitbitSiteBaseUrl = fitbitSiteBaseUrl;
	}

	public String getApiBaseUrl() {
		return apiBaseUrl;
	}

	public void setApiBaseUrl(String apiBaseUrl) {
		this.apiBaseUrl = apiBaseUrl;
	}

	public String getExampleBaseUrl() {
		return exampleBaseUrl;
	}

	public void setExampleBaseUrl(String exampleBaseUrl) {
		this.exampleBaseUrl = exampleBaseUrl;
	}

	public String getClientConsumerKey() {
		return clientConsumerKey;
	}

	public void setClientConsumerKey(String clientConsumerKey) {
		this.clientConsumerKey = clientConsumerKey;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public FitbitAPIClientService<FitbitApiClientAgent> getApiClientService() {
		return apiClientService;
	}

	public void setApiClientService(
			FitbitAPIClientService<FitbitApiClientAgent> apiClientService) {
		this.apiClientService = apiClientService;
	}

	private FitbitAPIClientService<FitbitApiClientAgent> apiClientService;


	private class RequestContext {

	    private LocalDate parsedLocalDate = new LocalDate();
	    private FitbitAPIClientService<FitbitApiClientAgent> apiClientService;
	    private LocalUserDetail ourUser;

	    @SuppressWarnings("unused")
		public LocalDate getParsedLocalDate() {
	        return parsedLocalDate;
	    }

	    public FitbitAPIClientService<FitbitApiClientAgent> getApiClientService() {
	        return apiClientService;
	    }

	    public void setApiClientService(FitbitAPIClientService<FitbitApiClientAgent> apiClientService) {
	        this.apiClientService = apiClientService;
	    }

	    public LocalUserDetail getOurUser() {
	        return ourUser;
	    }

	    public void setOurUser(LocalUserDetail ourUser) {
	        this.ourUser = ourUser;
	    }
	}


	@Override
	public boolean isAuthorized(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
        RequestContext context = new RequestContext();
        populate(context, request, response);
        if (!isAuthorized(context, request)) {
            return false;
        }
		return true;
	}

}
