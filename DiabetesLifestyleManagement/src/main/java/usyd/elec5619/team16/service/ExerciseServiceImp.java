package usyd.elec5619.team16.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import usyd.elec5619.team16.model.Exercise;
import usyd.elec5619.team16.model.FoodLog;
import usyd.elec5619.team16.model.User;

import com.fitbit.api.common.model.timeseries.Data;
import com.fitbit.api.common.model.timeseries.TimePeriod;
import com.fitbit.api.common.model.timeseries.TimeSeriesResourceType;

@Service
@Transactional
public class ExerciseServiceImp implements ExerciseService{
    protected Log log = LogFactory.getLog(getClass());
    
    @Autowired
	private FitbitService fitbitService;
    
	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * add exercise
	 * */
	@Override
	public void addExercise(Exercise exercise) {
		this.sessionFactory.getCurrentSession().save(exercise);
	}
	
	/*
	 * delete exercise
	 * */
	@Override
	public void deleteExercise(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Exercise exercise = (Exercise) currentSession.get(Exercise.class, id);
		
		currentSession.delete(exercise);
	}

	/*
	 * update exercise
	 * */
	@Override
	public void updateExercise(Exercise exercise) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(exercise);
	}

	/*
	 * get all exercise in a list
	 * */
	@SuppressWarnings("unchecked")
	@Override
	public List<Exercise> listExercise(User user) {
		Query query = this.sessionFactory.getCurrentSession().createQuery("FROM Exercise WHERE UserId="+user.getuId()+" ORDER BY date DESC");
		query.setMaxResults(30);
		return query.list();
	}
	
	public List<Exercise> getExerciseHistory(HttpServletRequest request, HttpServletResponse response,TimePeriod period, User user) {
		List<Data> calories = fitbitService.getActivityInfo(request, response, TimeSeriesResourceType.CALORIES_OUT_TRACKER, period);
		List<Data> weights = fitbitService.getActivityInfo(request, response, TimeSeriesResourceType.WEIGHT, period);
		
		List<Exercise> history = new ArrayList<Exercise>();
		
		for (int i = 0; i < calories.size(); i++) {
			Exercise exercise = new Exercise();
			exercise.setCaloriesBurned(calories.get(i).intValue());
			exercise.setWeight(weights.get(i).doubleValue());
			exercise.setCalorieTarget(2500);
			
			exercise.setUser(user);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date date = sdf.parse(calories.get(i).getDateTime());
				exercise.setDate(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			System.err.println("Fitbit data for 1 month:");
			System.err.println(i+": "+exercise.getDate()+" "+exercise.getCaloriesBurned()+" "+exercise.getWeight());
			history.add(exercise);
		}
		return history;
	}
	
	public Date getLastUpdate(User user) {
		List<Date> dates = this.sessionFactory.getCurrentSession().createQuery("select max(date) from Exercise where UserId="+user.getuId()).list();
		if (dates.size() > 0) {
			return dates.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void addExerciseHistory(HttpServletRequest request, HttpServletResponse response, TimePeriod period, User user) {
		Date latest = getLastUpdate(user);
		int days = 28;
		List<Exercise> history;
		
		if (latest != null) {
			Date now = new Date();
			DateTime date1 = new DateTime(now);
			DateTime date2 = new DateTime(latest);
			
			days = Days.daysBetween(date2, date1).getDays();
			if (days > 28) {
				days = 28;
			}
		}
		
		System.err.println("Retrieving fitbit information for past "+days+" days");
		
		history = getExerciseHistory(request, response, TimePeriod.ONE_MONTH, user);
		for (int i = history.size()-days; i < history.size(); i++) {
			Exercise exercise = history.get(i);
			System.err.println(i+": "+exercise.getDate()+" "+exercise.getCaloriesBurned()+" "+exercise.getWeight());
			addExercise(exercise);
		}
	}

}
