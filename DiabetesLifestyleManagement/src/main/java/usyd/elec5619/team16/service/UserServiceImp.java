package usyd.elec5619.team16.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fitbit.api.client.LocalUserDetail;
import com.fitbit.api.common.model.activities.Activities;
import com.fitbit.api.common.model.user.UserInfo;

import usyd.elec5619.team16.model.BloodSugar;
import usyd.elec5619.team16.model.Challenge;
import usyd.elec5619.team16.model.Exercise;
import usyd.elec5619.team16.model.FoodLog;
import usyd.elec5619.team16.model.User;

@Service
@Transactional
public class UserServiceImp implements UserService{
	
	protected Log log = LogFactory.getLog(getClass());
    
    @Autowired
	private FitbitService fitbitService;
    
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addUser(User user) {
		this.sessionFactory.getCurrentSession().save(user);		
	}
	
	
    public void addUserChallenge(User user, String challenge)
    {
		Challenge c = new Challenge();
		c.setUser(user);
    	c.setText(challenge);
    	//c.setDate(DateTime.now().toDate().toString());
    	
    	this.sessionFactory.getCurrentSession().save(c);
    }


	@Override
	public void deleteUser(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, id);
		
		currentSession.delete(user);
	}

	@Override
	public void updateUser(User user) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(user);
	}

	@Override
	public User getUser(int id) {
		List<User> results = this.sessionFactory.getCurrentSession().createQuery("FROM User WHERE Id="+id).list();
		if (results.size() > 0) {
			return (User) results.get(0);
		}
		else {
			return null;
		}
	}

	@Override
	public User getUser(String email) {
		List<User> results = this.sessionFactory.getCurrentSession().createQuery("FROM User WHERE email='"+email+"'").list();
		if (results.size() > 0) {
			return (User) results.get(0);
		}
		else {
			return null;
		}
	}
	
	@Override
	public Boolean authenticateUser(String email, String pw) {
		User user = getUser(email);
		if (user == null) {
			return false;
		}
		else if (user.getPW().equals(pw) && !pw.equals(null)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void updateUserInfo(HttpServletRequest request, HttpServletResponse response, String email)
	{
		UserInfo userInfo = fitbitService.getUserInfo(request, response);
		Activities activityGoals = fitbitService.getActivityGoals(request, response);
		User user = getUser(email);
		user.setName(userInfo.getFullName());
		user.setHeight(userInfo.getHeight());
		user.setWeight(userInfo.getWeight());
		user.setAvatar(userInfo.getAvatar());
		user.setDateOfBirth(userInfo.getDateOfBirth());
		user.setCaloriesTarget(activityGoals.getActivityGoals().getCaloriesOut());
		user.setCurrentCalories(activityGoals.getSummary().getCaloriesOut());
		user.setStepsTarget(activityGoals.getActivityGoals().getSteps());
		user.setStepsTaken(activityGoals.getSummary().getSteps());

		//System.err.print(userInfo.getAvatar());
		
		updateUser(user);		
	}

	public List<Challenge> getUserChallenges(User user) 
	{
		@SuppressWarnings("unchecked")
		List<Challenge> list = this.sessionFactory.getCurrentSession().createQuery("FROM Challenge WHERE userId='"+user.getuId()+"'").list();
		
		return list;
	}

}
