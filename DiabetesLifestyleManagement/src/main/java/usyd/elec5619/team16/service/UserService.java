package usyd.elec5619.team16.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import usyd.elec5619.team16.model.BloodSugar;
import usyd.elec5619.team16.model.Challenge;
import usyd.elec5619.team16.model.User;

public interface UserService {
    public void addUser(User user);
    public void addUserChallenge(User user, String challenge);
    public void deleteUser(int id);
	public void updateUser(User user);
	public void updateUserInfo(HttpServletRequest request, HttpServletResponse response, String email);
    public User getUser(int id);
    public User getUser(String email);
    public List<Challenge> getUserChallenges(User user);
    public Boolean authenticateUser(String email, String pw);
}
