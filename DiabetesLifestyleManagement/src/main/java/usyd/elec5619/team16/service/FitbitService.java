package usyd.elec5619.team16.service;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.LocalDate;

import com.fitbit.api.common.model.activities.Activities;
import com.fitbit.api.common.model.foods.Food;
import com.fitbit.api.common.model.sleep.Sleep;
import com.fitbit.api.common.model.timeseries.Data;
import com.fitbit.api.common.model.timeseries.TimePeriod;
import com.fitbit.api.common.model.timeseries.TimeSeriesResourceType;
import com.fitbit.api.common.model.user.UserInfo;

public interface FitbitService {
	String authorize(HttpServletRequest request, HttpServletResponse response,String completeRequest);
	
	List<Food> searchFoods(HttpServletRequest request, HttpServletResponse response,String searchName);
	
	boolean isAuthorized(HttpServletRequest request, HttpServletResponse response);
	void completeAuthorization(HttpServletRequest request, HttpServletResponse response);
	List<Data> getActivityInfo(HttpServletRequest request, HttpServletResponse response, TimeSeriesResourceType resourceType, TimePeriod period);
	public Sleep getSleep(HttpServletRequest request, HttpServletResponse response, LocalDate  date);
	public UserInfo getUserInfo(HttpServletRequest request, HttpServletResponse response);
	Activities getActivityGoals(HttpServletRequest request, HttpServletResponse response);
}
