package usyd.elec5619.team16.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fitbit.api.common.model.timeseries.TimePeriod;

import usyd.elec5619.team16.model.SleepGoal;
import usyd.elec5619.team16.model.SleepLog;
import usyd.elec5619.team16.model.User;

public interface SleepService {
    public void addSleepLog(SleepLog SleepLog);
    
    public void deleteSleepLog(int id);
    
	public void updateSleepLog(SleepLog sleepLog);
	
	public List<SleepLog> getSleepLogByTime(HttpServletRequest request, HttpServletResponse response,TimePeriod period,User currentUser);
	
    public List<SleepLog> listSleepLog();
    
    public List<SleepGoal> getSleepGoals(User user);
    
    public void addSleepGoal(SleepGoal sleepGoal);
    
    public void deleteSleepgoal(int id);
    

}
