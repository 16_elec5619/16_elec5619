package usyd.elec5619.team16.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import usyd.elec5619.team16.model.BloodSugar;
import usyd.elec5619.team16.model.User;

@Service
@Transactional
public class BloodServiceImp implements BloodService{

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addReading(BloodSugar bloodSugar) {
		this.sessionFactory.getCurrentSession().save(bloodSugar);
	}

	@Override
	public void deleteBloodSugar(int id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		BloodSugar reading = (BloodSugar) currentSession.get(BloodSugar.class, id);
		
		currentSession.delete(reading);
	}

	@Override
	public void updateBloodSugar(BloodSugar bloodSugar) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(bloodSugar);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BloodSugar> listBloodSugar(User user) {
		return this.sessionFactory.getCurrentSession().createQuery("FROM BloodSugar WHERE UserId="+user.getuId()+"").list();
	}

	@Override
	public BloodSugar getLatestReading(User user) {
		List<BloodSugar> readings = this.sessionFactory.getCurrentSession().createQuery("from BloodSugar WHERE UserId="+user.getuId()+" order by date DESC LIMIT 1").list();
		if (readings.size() > 0) {
			return readings.get(0);
		}
		else {
			return null;
		}
	}
}
