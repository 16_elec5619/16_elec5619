package usyd.elec5619.team16.service;

import java.util.List;

import usyd.elec5619.team16.model.BloodSugar;
import usyd.elec5619.team16.model.User;
//import usyd.elec5619.team16.model.User;

public interface BloodService {
    public void addReading(BloodSugar bloodSugar);
    public void deleteBloodSugar(int id);
	public void updateBloodSugar(BloodSugar bloodSugar);
    public List<BloodSugar> listBloodSugar(User user);
    public BloodSugar getLatestReading(User user);
}
