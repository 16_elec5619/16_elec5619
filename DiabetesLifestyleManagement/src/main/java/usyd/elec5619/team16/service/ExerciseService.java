package usyd.elec5619.team16.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fitbit.api.common.model.timeseries.TimePeriod;

import usyd.elec5619.team16.model.Exercise;
import usyd.elec5619.team16.model.User;

public interface ExerciseService {
	
	public void addExercise(Exercise exercise);
	public void deleteExercise(int id);
	public void updateExercise(Exercise exercise);
	public List<Exercise> listExercise(User user);
	public Date getLastUpdate(User user);
	public List<Exercise> getExerciseHistory(HttpServletRequest request, HttpServletResponse response,TimePeriod period, User user);
	public void addExerciseHistory(HttpServletRequest request, HttpServletResponse response,TimePeriod period, User user);
}
