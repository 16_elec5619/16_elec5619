package usyd.elec5619.team16.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import usyd.elec5619.team16.model.FoodLog;

@Service
@Transactional
public class FoodLogServiceImp implements FoodLogService{

	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * add food log
	 * */
	@Override
	public void addFoodLog(FoodLog foodLog) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(foodLog);
	}
	
	/*
	 * delete food log
	 * */
	@Override
	public void deleteFoodLog(int id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		FoodLog product = (FoodLog) currentSession.get(FoodLog.class, id);
		
		currentSession.delete(product);
	}

	/*
	 * update food log
	 * */
	@Override
	public void updateFoodLog(FoodLog foodLog) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(foodLog);
	}

	/*
	 * get all food log and return with a list
	 * */
	@SuppressWarnings("unchecked")
	@Override
	public List<FoodLog> listFoodLog() {
		// TODO Auto-generated method stub
		return this.sessionFactory.getCurrentSession().createQuery("FROM FoodLog").list();
	}
}
