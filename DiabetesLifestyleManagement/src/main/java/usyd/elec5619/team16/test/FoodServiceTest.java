package usyd.elec5619.team16.test;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import usyd.elec5619.team16.model.FoodLog;
import usyd.elec5619.team16.service.FitbitService;
import usyd.elec5619.team16.service.FoodLogService;

import com.fitbit.api.common.model.foods.Food;

@Controller
public class FoodServiceTest {

	@Autowired
	private FitbitService fitbitService; 
	
	@Autowired
	private FoodLogService foodLogService;
	
	/* food add */
	private List<Food> foods = null;
	
	/*
	 * food service test
	 * 1.food search test
	 * */
	@RequestMapping("/testFoodServicer")
	public String addFoodLog(Model model, HttpServletRequest request, HttpServletResponse response){
		/* test log */
		String testlog = "";
		
		/* food search test */
		testlog +="food search test:<br>";
		String searchName = "Tomato";
		foods = fitbitService.searchFoods(request, response, searchName);
		if(foods == null)
			return "redirect:authorize";
		for(Food log:foods){
			testlog +=log.getName() + "<br>";
		}
		
		/* add food log test*/
		testlog +="add food log test:<br>";
		FoodLog foodLog = new FoodLog();
		foodLog.setFoodName("Tomato");
		foodLog.setCalories(100);
		foodLogService.addFoodLog(new FoodLog());
		
		/* get food logs test*/
		testlog +="list food log test:<br>";
		List<FoodLog> foodLogs = foodLogService.listFoodLog();
		for(FoodLog foodLog1:foodLogs){
			testlog +=foodLog1.getFoodName() + "_____" + foodLog1.getCalories() + "<br>";
		}
		
		model.addAttribute("log", testlog);
		return "test/testFoodLog";
	}
	
	
}
