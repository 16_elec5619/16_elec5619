package usyd.elec5619.team16.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import usyd.elec5619.team16.model.FoodLog;
import usyd.elec5619.team16.service.FitbitService;
import usyd.elec5619.team16.service.FoodLogService;

import com.fitbit.api.common.model.foods.Food;

@Controller
public class FoodController {


	@Autowired
	private FitbitService fitbitService; 
	
	@Autowired
	private FoodLogService foodLogService;
	
	/* food add */
	private List<Food> foods = null;
	
	/*
	 * add food log
	 * */
	@RequestMapping("/foodAddLog")
	public String addFoodLog(Model model){
		return "food/foodLog";
	}
	
	/*
	 * handle food search request
	 * */
	@RequestMapping("/foodSearchFood")
	public String searchFood(Model model, HttpServletRequest request, HttpServletResponse response){
		String searchName = (String) request.getParameter("searchName");
		foods = fitbitService.searchFoods(request, response, searchName);
		if(foods == null)
			return "redirect:authorize";
		model.addAttribute("foods", foods);
		model.addAttribute("foodLog",new FoodLog());
		return "food/foodLog";
	}
	
	/*
	 * calculate calories according to quantity and units*/
	@RequestMapping("/foodCalculateCalories")
	public String caculateCalories(FoodLog foodLog, HttpServletRequest request){
		foodLog.setCalories((double)foodLog.getQuantity() * (double)foodLog.getCaloriesPerUnit() / (double)foodLog.getDefaultServingSize());
		foodLog.setDate(new Date());
		foodLogService.addFoodLog(foodLog);  //update log to database
		return "redirect:foodLogTrack";
	}
	
	/* 
	 * food track, shows with line chart and table*/
	@RequestMapping("/foodLogTrack")
	public String logTrack(Model model){
		List<FoodLog> foodLogs = foodLogService.listFoodLog();

		Date date = new Date();
		date.getMinutes();

		model.addAttribute("foodLogs", foodLogs);
		return "food/foodLogTrack";
	}
	
}
