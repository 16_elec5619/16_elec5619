package usyd.elec5619.team16.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

//import org.json.simple.JSONOBJECT;




import com.fitbit.api.common.model.timeseries.Data;
import com.fitbit.api.common.model.timeseries.TimePeriod;
import com.fitbit.api.common.model.timeseries.TimeSeriesResourceType;

import usyd.elec5619.team16.model.Exercise;
import usyd.elec5619.team16.model.User;
import usyd.elec5619.team16.service.ExerciseService;
import usyd.elec5619.team16.service.FitbitService;

@Controller
public class ExerciseController {
	
	@Autowired
	private ExerciseService exerciseService;
	
	@RequestMapping("/exerciseHome")
	public String exerciseOverview(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session){
		User user = (User) session.getAttribute("currentUser");
		exerciseService.addExerciseHistory(request, response, TimePeriod.ONE_MONTH, user);
		List<Exercise> exercises = exerciseService.listExercise(user);
		model.addAttribute("exercises", exercises);
		return "exercise/exerciseHome";
	}
	
}
