package usyd.elec5619.team16.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import usyd.elec5619.team16.service.FitbitService;

@Controller
public class FitBitController {
	@Autowired
	private FitbitService fitbitService; 
	
	@RequestMapping("/authorize")
	public String authorize(HttpServletRequest request, HttpServletResponse response) {	 
		return fitbitService.authorize(request, response, "completeAuthorize");
	}
	
	@RequestMapping("/completeAuthorize")
	public String completeAuthorize(HttpServletRequest request, HttpServletResponse response) {
		fitbitService.completeAuthorization(request, response);
		return "redirect:dashboard";
	}
	
}

