package usyd.elec5619.team16.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fitbit.api.common.model.timeseries.TimePeriod;

import usyd.elec5619.team16.model.Challenge;
import usyd.elec5619.team16.model.Exercise;
import usyd.elec5619.team16.model.User;
import usyd.elec5619.team16.service.ExerciseService;
import usyd.elec5619.team16.service.FitbitService;
import usyd.elec5619.team16.service.UserService;

@Controller
public class HomeController {
	@Autowired
	private FitbitService fitbitService; 
	
	@Autowired
	private UserService userService; 
	
	@Autowired
	private ExerciseService exerciseService; 
	
	@RequestMapping("/")
	public String base()
	{	
		return "redirect:introduction";
	}
	
	@RequestMapping("/introduction")
	public String introduction()
	{	
		return "home/home";
	}
	
	@RequestMapping("/login")
	public String login()
	{	
		return "home/login";
	}
	
	@RequestMapping("/signup")
	public String signup(){
		
		return "home/signup";
	}
	
	@RequestMapping(value="/signupUser", method=RequestMethod.POST)
	public String signupUser(Model model, HttpServletRequest request, HttpServletResponse response){
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		
		User user = new User();
		user.setEmail(email);
		user.setPW(password);
		user.setName(name);
		user.setGender(gender);
		
		userService.addUser(user);
		return "redirect:login";
	}
		
	@RequestMapping("/dashboard")
	public String dashboard(HttpSession session, HttpServletRequest request, HttpServletResponse response, Model model)
	{
		//gated access based on the user login
		User currentuser = (User) session.getAttribute("currentUser");

		//profile data
		userService.updateUserInfo(request, response, currentuser.getEmail());
		String fullname = currentuser.getName();
		double height = currentuser.getHeight();
		double weight = currentuser.getWeight();
		String avatar = currentuser.getAvatar();
		double activity = currentuser.getActivityPerformance();
		double calories = currentuser.getCaloriesPerformance();
		String[] challenge = currentuser.getDailychallenge(activity, calories);
		userService.addUserChallenge(currentuser, challenge[0]);
		List<Exercise> exercises = exerciseService.getExerciseHistory(request, response, TimePeriod.SEVEN_DAYS, currentuser);
		List<Challenge> challenges = userService.getUserChallenges(currentuser);

		model.addAttribute("name", fullname);
		model.addAttribute("height", height);
		model.addAttribute("weight", weight);
		model.addAttribute("birth", currentuser.getDateOfBirth());
		model.addAttribute("StepsTarget", currentuser.getStepsTarget());
		model.addAttribute("CaloriesTarget", currentuser.getCaloriesTarget());		
		model.addAttribute("avatar", avatar);
		model.addAttribute("activity", activity);
		model.addAttribute("calories", calories);
		model.addAttribute("challenge", challenge[0]);
		model.addAttribute("place", challenge[1]);
		model.addAttribute("challenges0", challenges.get(0).getText());
		model.addAttribute("challenges1", challenges.get(1).getText());
		model.addAttribute("challenges2", challenges.get(2).getText());
		
		String s = "";
		double d = Math.round(100*((exercises.get(5).getCaloriesBurned().doubleValue())/(exercises.get(5).getCalorieTarget().doubleValue())));
		if(d == calories)
		{
			s = "Doing Good!";
		}
		if(d < calories)
		{
			s = "Doing Better!";
		}
		else
		{
			s = "Work Harder";
		}
		model.addAttribute("statement", s);
		model.addAttribute("comparison", (calories-d));


		if (currentuser != null){
			return "home/homeDashboard";
		}
		else
			return "redirect:login";
	}
	
	/*@RequestMapping("/authenticateUser")
	public String authenticateUser(HttpServletRequest request, HttpServletResponse response){
		String email = request.getParameter("email");
		String password = request.getParameter("password");

		System.err.println(email+ " " +password);
		System.err.println(userService.authenticateUser(email, password));
		
		if (userService.authenticateUser(email, password)) {
			
			return "redirect:validateLogin";
		}
		else {
			System.err.println("not authenticated");
			return "redirect:login";
		}
	}*/
	
	@RequestMapping("/validateLogin")
	public String validateLogin(HttpServletRequest request, HttpServletResponse response, HttpSession session){
		String password = (String) request.getParameter("password");
		String email = (String) request.getParameter("email");
		
		if(userService.authenticateUser(email, password)){
			User currentUser = userService.getUser(email);
			session.setAttribute("currentUser", currentUser);
			System.out.println("Current user set: " + currentUser.getEmail());
			
			if(!fitbitService.isAuthorized(request, response))
				return "redirect:authorize";
			return "redirect:dashboard";
		}
		else {
			return"redirect:login";
		}
		
	}
	
	
}
