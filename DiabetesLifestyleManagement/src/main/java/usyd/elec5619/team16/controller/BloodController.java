package usyd.elec5619.team16.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import usyd.elec5619.team16.model.BloodSugar;
import usyd.elec5619.team16.model.User;
import usyd.elec5619.team16.service.BloodService;
import usyd.elec5619.team16.service.FoodLogService;

@Controller
public class BloodController {
	
	@Autowired
	private BloodService bloodService;
	
	@RequestMapping("/bloodHome")
	public String bloodOverview(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session){
		User user = (User) session.getAttribute("currentUser");
		BloodSugar latest = bloodService.getLatestReading(user);
		List<BloodSugar> readings = bloodService.listBloodSugar(user);
		model.addAttribute("latest", latest);
		model.addAttribute("readings", readings);
		return "blood/bloodHome";
	}
	
	@RequestMapping(value="/addReading", method=RequestMethod.POST)
	public String addReading(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session){
		BloodSugar reading = new BloodSugar();
		reading.setSugarLevel(Double.parseDouble(request.getParameter("sugarLevel")));
		reading.setDate(new Date());
		User user = (User) session.getAttribute("currentUser");
		reading.setUser(user);
		
		bloodService.addReading(reading);
		return "redirect:/bloodHome";
	}
}
