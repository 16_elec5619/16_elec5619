package usyd.elec5619.team16.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fitbit.api.common.model.timeseries.TimePeriod;
import com.fitbit.api.common.model.timeseries.Data;
import com.fitbit.api.common.model.timeseries.TimeSeriesResourceType;
import com.fitbit.api.model.APIResourceCredentials;

import usyd.elec5619.team16.model.SleepGoal;
import usyd.elec5619.team16.model.SleepLog;
import usyd.elec5619.team16.model.User;
import usyd.elec5619.team16.service.FitbitService;
import usyd.elec5619.team16.service.SleepService;


@Controller
@RequestMapping("/sleep/**")
public class SleepController {
	
	@Autowired
	private FitbitService fitbitService;
	
	@Autowired
	private SleepService sleepservice;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String overview(Model model, HttpServletRequest request, HttpServletResponse response,HttpSession session){
		User currentUser = (User) session.getAttribute("currentUser");
		if (currentUser != null){
			
			List<SleepGoal> allGoals = sleepservice.getSleepGoals(currentUser);
			List<SleepGoal> goalLists =  new ArrayList<SleepGoal>();
			
			for(SleepGoal x :allGoals){
				if(x.getUser().getuId().equals(currentUser.getuId()))
					goalLists.add(x);
			}
			SleepGoal currentGoal = goalLists.get(goalLists.size()-1);
			//String minutesAsleep = fitbitService.getActivityInfo(request, response, TimeSeriesResourceType.MINUTES_ASLEEP, TimePeriod.ONE_WEEK).get(0).getValue();
			List<SleepLog> logs = sleepservice.getSleepLogByTime(request, response, TimePeriod.ONE_MONTH, currentUser);
			//List<Data> minuteAsleeps = fitbitService.getActivityInfo(request, response, TimeSeriesResourceType.MINUTES_ASLEEP, TimePeriod.ONE_MONTH);
			//List<Data> efficiencies = fitbitService.getActivityInfo(request, response,  TimeSeriesResourceType.EFFICIENCY, TimePeriod.ONE_MONTH);
			System.out.println(logs.size());
			
			
			model.addAttribute("sleepGoal",currentGoal);
			model.addAttribute("logs", logs);
			return "Sleep/SleepHome";
		}
		else{
			return "redirect:/login";
		}

	}
	
	@RequestMapping(value="/settings", method = RequestMethod.GET)
	public String getSettings(Model model, HttpServletRequest request, HttpServletResponse response,HttpSession session){
		if (session.getAttribute("currentUser")!= null){
	
			User currentUser = (User) session.getAttribute("currentUser");
			
			List<SleepGoal> allGoals = sleepservice.getSleepGoals(currentUser);
			List<SleepGoal> goalLists =  new ArrayList<SleepGoal>();
			
			for(SleepGoal x :allGoals){
				if(x.getUser().getuId().equals(currentUser.getuId()))
					goalLists.add(x);
			}
			Collections.reverse(goalLists);
			model.addAttribute("goalLists",goalLists);
			
			Date date = new Date();
			SleepGoal predata = new SleepGoal();
			predata.setDateCreated(date);
			predata.setUser(currentUser);
			
			model.addAttribute("sleepGoal", predata);
			return "Sleep/SleepSetting";
		}
		else{
			return "redirect:/login";
		}
		
	}
	

	@RequestMapping(value="/settings", method = RequestMethod.POST)
	public String updateSettings(@ModelAttribute("settingsForm") SleepGoal sleepGoal ,Model model, HttpServletRequest request, HttpServletResponse response,HttpSession session){
		
		if (session.getAttribute("currentUser")!= null){			
			User currentUser = (User) session.getAttribute("currentUser");
			
			sleepGoal.setUser(currentUser);		
			sleepservice.addSleepGoal(sleepGoal);

			List<SleepGoal> allGoals = sleepservice.getSleepGoals(currentUser);
			List<SleepGoal> goalLists =  new ArrayList<SleepGoal>();
			
			for(SleepGoal x :allGoals){
				if(x.getUser().getuId().equals(currentUser.getuId()))
					goalLists.add(x);
			}
			Collections.reverse(goalLists);
			model.addAttribute("goalLists",goalLists);
			model.addAttribute("hours",sleepGoal);
			
			Date date = new Date();
			SleepGoal predata = new SleepGoal();
			predata.setDateCreated(date);
			predata.setUser(currentUser);
			model.addAttribute("sleepGoal", predata);
			
//			System.out.println(sleepGoal.getGoalHours());
//			System.out.println(sleepGoal.getDateCreated());
//			System.out.println(sleepGoal.getUser().getEmail());
			return "Sleep/SleepSetting" ;
		}
		else {
			return "redirect:/login";
		}

	}
	
	public String updateLogs(){
		return "home";
	}
	

	
}

