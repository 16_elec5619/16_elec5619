package usyd.elec5619.team16.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.joda.time.DateTime;

@Entity
public class SleepMinuteData {
	
	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;
	private DateTime datetime;
	private int value;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id")
	private SleepLog associatedSleepLog;
	
	public DateTime getDatetime() {
		return datetime;
	}
	public void setDatetime(DateTime datetime) {
		this.datetime = datetime;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
}
