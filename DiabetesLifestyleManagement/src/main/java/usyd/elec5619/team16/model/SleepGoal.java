package usyd.elec5619.team16.model;

import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;


@Entity
@Table(name="SleepGoal")
public class SleepGoal {
	
    @ManyToOne
    @JoinColumn(name="UserId")
    private User user;

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;
	
	@Column(name="Date")
    private int goalHours;
	
	private boolean isActive;
	
	private Date dateCreated;
	
	
	//get and sets

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGoalHours() {
		return goalHours;
	}

	public void setGoalHours(int goalHours) {
		this.goalHours = goalHours;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	

	
	
}
