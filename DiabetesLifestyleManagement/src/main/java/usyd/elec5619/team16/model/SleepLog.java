package usyd.elec5619.team16.model;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="SleepLogs")
public class SleepLog {
	
	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;
	@ManyToOne
    @JoinColumn(name="UserId")
	private User user;
	private boolean isMainSleep;
	private int logId;
	private int efficiency;
	
	private Date date;
	private Time time;
	
	private int duration;
	private int minutesToFallAsleep;
	private int minutesAsleep;
	private int minutesAwake;
	private int minutesAfterWakeup;
	private int awakeCount;
	private int awakeDuration;
	private int restlessCount;
	private int restlessDuration;
	private int timeInBed;
	
	//need to re-confirm
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "associatedSleepLog")
	private List<SleepMinuteData> minuteDatas;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isMainSleep() {
		return isMainSleep;
	}

	public void setMainSleep(boolean isMainSleep) {
		this.isMainSleep = isMainSleep;
	}

	public int getLogId() {
		return logId;
	}

	public void setLogId(int logId) {
		this.logId = logId;
	}

	public int getEfficiency() {
		return efficiency;
	}

	public void setEfficiency(int efficiency) {
		this.efficiency = efficiency;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getMinutesToFallAsleep() {
		return minutesToFallAsleep;
	}

	public void setMinutesToFallAsleep(int minutesToFallAsleep) {
		this.minutesToFallAsleep = minutesToFallAsleep;
	}

	public int getMinutesAsleep() {
		return minutesAsleep;
	}

	public void setMinutesAsleep(int minutesAsleep) {
		this.minutesAsleep = minutesAsleep;
	}

	public int getMinutesAwake() {
		return minutesAwake;
	}

	public void setMinutesAwake(int minutesAwake) {
		this.minutesAwake = minutesAwake;
	}

	public int getMinutesAfterWakeup() {
		return minutesAfterWakeup;
	}

	public void setMinutesAfterWakeup(int minutesAfterWakeup) {
		this.minutesAfterWakeup = minutesAfterWakeup;
	}

	public int getAwakeCount() {
		return awakeCount;
	}

	public void setAwakeCount(int awakeCount) {
		this.awakeCount = awakeCount;
	}

	public int getAwakeDuration() {
		return awakeDuration;
	}

	public void setAwakeDuration(int awakeDuration) {
		this.awakeDuration = awakeDuration;
	}

	public int getRestlessCount() {
		return restlessCount;
	}

	public void setRestlessCount(int restlessCount) {
		this.restlessCount = restlessCount;
	}

	public int getRestlessDuration() {
		return restlessDuration;
	}

	public void setRestlessDuration(int restlessDuration) {
		this.restlessDuration = restlessDuration;
	}

	public int getTimeInBed() {
		return timeInBed;
	}

	public void setTimeInBed(int timeInBed) {
		this.timeInBed = timeInBed;
	}

	public List<SleepMinuteData> getMinuteData() {
		return minuteDatas;
	}

	public void setMinuteData(List<SleepMinuteData> minuteData) {
		this.minuteDatas = minuteData;
	}
	
	
}
