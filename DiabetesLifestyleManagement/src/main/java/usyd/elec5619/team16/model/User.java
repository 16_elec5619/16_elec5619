package usyd.elec5619.team16.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.joda.time.DateTime;
import org.springframework.ui.Model;

@Entity
@Table(name="User")
public class User 
{
	@Id
	@GeneratedValue
	@Column(name="Id")
	private Integer Id;
	
	@Column(name="Name")
	private String name;
	
	@Column(name="Email")
	private String email;
	
	@Column(name="Password")
	private String password;
	
	@Column(name="Gender")
	private String gender;

	@Column(name="DoB")
	private String dateOfBirth;
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	@Column(name="Height")
	private double height;
	
	@Column(name="Weight")
	private double weight;
	
	@Column(name="Avatar")
	private String avatar;
	
	@Column(name="ActivityGoal")
	private double stepsTarget;
	
	@Column(name="StepsDone")
	private double stepsTaken;
	
	@Column(name="DietGoal")
	private double caloriesTarget;
	
	@Column(name="CaloriesBurnt")
	private double currentCalories;
	
	//getters and setters
	
	public double getStepsTaken()
	{
		return stepsTaken;
	}
	public void setStepsTaken(double stepsTaken) {
		this.stepsTaken = stepsTaken;
	}
	public double getCurrentCalories() {
		return currentCalories;
	}
	public void setCurrentCalories(double currentCalories) {
		this.currentCalories = currentCalories;
	}
	
	public double getStepsTarget() {
		return stepsTarget;
	}
	public void setStepsTarget(double stepsTarget) {
		this.stepsTarget = stepsTarget;
	}
	public double getCaloriesTarget() {
		return caloriesTarget;
	}
	public void setCaloriesTarget(double caloriesTarget) {
		this.caloriesTarget = caloriesTarget;
	}
	
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Integer getuId() {
		return Id;
	}
	public void setuId(Integer Id) {
		this.Id = Id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPW() {
		return password;
	}
	public void setPW(String password) {
		this.password = password;
	}
	

//	public Set<FoodLog> getFoodLogs() {
//		return foodLogs;
	
	public double getWeight() {
		return this.weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getHeight() {
		return this.height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public double getActivityPerformance()
	{
		return Math.round(100*(getStepsTaken()/getStepsTarget()));
	}
	public double getCaloriesPerformance()
	{
		return Math.round(100*(getCurrentCalories()/getCaloriesTarget()));
	}
	public String[] getDailychallenge(double a, double b) 
	{
		String[] challengeA = {"Dare to go to watch a movie and at least do","cenima"};
		String[] challengeB = {"Dare to indulge yourself with a 300 ml smoothie", "cafe"};
		String[] challengeC = {"You gotta go out now and do at least","park"};
		String[] challengeD = {"Go for a short walk and get some air","park"};
		if(a <= 50 && b <= 50)
		{
			if(a <= b)
			{
				double d = Math.round(0.30 * getStepsTarget()); 
				challengeC[0] = challengeC[0] + " " + d + " steps!";
				return challengeC;
			}
			else
			{
				return challengeD;
			}
		}
		else
		{
			if(a <= b)
			{
				return challengeB;
			}
			else
			{
				double d = Math.round(0.20 * getStepsTarget()); 
				challengeA[0] = challengeA[0] + " " + d + " steps!";
				return challengeA;
			}
		}
	}

	
}
