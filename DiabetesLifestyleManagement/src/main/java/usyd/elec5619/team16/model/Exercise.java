package usyd.elec5619.team16.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Exercise")
public class Exercise {
	
	@Id
	@GeneratedValue
	@Column(name="Id")
	private Integer Id;
	
	@Column(name="Date")
	private Date date;
	
	@Column(name="Weight")
	private Double weight;
	
	@Column(name="CaloriesBurned")
	private Integer caloriesBurned;
	
	@Column(name="CalorieTarget")
	private Integer calorieTarget = 2500;
	
	@ManyToOne
    @JoinColumn(name="UserId")
    private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		this.Id = id;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
		
	public Integer getCaloriesBurned() {
		return caloriesBurned;
	}
	public void setCaloriesBurned(Integer calories) {
		this.caloriesBurned = calories;
	}
	
	public Integer getCalorieTarget() {
		return calorieTarget;
	}
	public void setCalorieTarget(Integer calories) {
		this.calorieTarget = calories;
	}
	
}
