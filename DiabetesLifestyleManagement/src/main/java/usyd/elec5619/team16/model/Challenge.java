package usyd.elec5619.team16.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name="Challenge")
public class Challenge
{
	@Id
	@GeneratedValue
	@Column(name="Id")
	private Integer Id;
		
	@ManyToOne
    @JoinColumn(name="UserId")
    private User user;
		
	@Column(name="Text")
	private String text;

	@Column(name="Date")
	private DateTime date;
	
	public Integer getId() 
	{
		return Id;
	}

	public void setId(Integer id) 
	{
		Id = id;
	}

	public User getUser() 
	{
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDate() {
		return date.toDate().toString();
	}

	public void setDate(String dateTime) {
		this.date = DateTime.parse(dateTime);
	}
}

