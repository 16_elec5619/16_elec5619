package usyd.elec5619.team16.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="BloodSugar")
public class BloodSugar {
	@Id
	@GeneratedValue
	@Column(name="Id")
	private Integer id;
		
	@Column(name="SugarLevel")
	private Double sugarLevel;
	
	@Column(name="Date")
	private Date date;
	
	@Column(name="Status")
	private String status;
	
	
	@ManyToOne
    @JoinColumn(name="UserId")
    private User user;
	
	
	// Getters and setters
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Double getSugarLevel() {
		return sugarLevel;
	}
	public void setSugarLevel(Double sugarLevel) {
		this.sugarLevel = sugarLevel;
		setStatus(sugarLevel);
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getStatus() {
		return status;
	}
	private void setStatus(Double sugarLevel) {
		if (sugarLevel > 8.5) {
			this.status = "High";
		} else if (sugarLevel < 4.0) {
			this.status = "Low";
		} else if (sugarLevel > 4.0 && sugarLevel < 8.5) {
			this.status = "Normal";
		}
	}
}