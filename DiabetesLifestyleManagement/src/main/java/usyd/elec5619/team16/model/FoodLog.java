package usyd.elec5619.team16.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="FoodLog")
public class FoodLog {
	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;
	
	@Column(name="FoodId")
	private long foodId;

	@Column(name="FoodName")
	private String foodName;
	
	@Column(name="Calories")
	private double calories;
	
	@Column(name="Quantity")
	private double quantity;
	
	@Column(name="CaloriesPerUnit")
	private double caloriesPerUnit;
	
	@Column(name="DefaultServingSize")
	private double defaultServingSize;
	
	@Column(name="Date")
	private Date date;
	
    @ManyToOne
    @JoinColumn(name="UserId")
    private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public double getDefaultServingSize() {
		return defaultServingSize;
	}
	public void setDefaultServingSize(double defaultServingSize) {
		this.defaultServingSize = defaultServingSize;
	}
	public double getCaloriesPerUnit() {
		return caloriesPerUnit;
	}
	public void setCaloriesPerUnit(double caloriesPerUnit) {
		this.caloriesPerUnit = caloriesPerUnit;
	}

	
	public long getFoodId() {
		return foodId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setFoodId(long foodId) {
		this.foodId = foodId;
	}

	public String getFoodName() {
		return foodName;
	}
	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}
	public double getCalories() {
		return calories;
	}
	public void setCalories(double calories) {
		this.calories = calories;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
		
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
}
