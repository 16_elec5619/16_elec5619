<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html><head>
    <meta charset="utf-8">
    <title>GetFit | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Carlos Alvarez - Alvarez.is">

    <!-- Le styles -->

    <link href="<c:url value="/resources/assets/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/main.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/font-style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/flexslider.css"/>" rel="stylesheet">
    
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>

    <style type="text/css">
      body {
        padding-top: 60px;
      }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

  	<!-- Google Fonts call. Font Used Open Sans & Raleway -->
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
  	<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    
  </head>
  <body>
  
  	<!-- NAVIGATION MENU -->

    <div class="navbar-nav navbar-inverse navbar-fixed-top">
        <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="validateLogin"><img src="<c:url value="/resources/assets/img/logo30.png"/>" alt=""> GetFit</a>
        </div> 
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li ><a href="foodLogTrack"><i class="icon-home icon-white"></i>Log</a></li>
              <li class="active"><a href="foodAddLog"><i class="icon-lock icon-white"></i> Add Log</a></li>

            </ul>
          </div><!--/.nav-collapse -->
        </div>
    </div>

    <div  style = "background-color: white;" class="container container-fluid">
		<!-- body -->
		
					<div>
					<div>
						<h1 class="page-header">Food Search</h1>
						<form class="form-inline" role="form" action="foodSearchFood">
							<input type="text" placeholder="Food Name" class="form-control"
								name="searchName">
							<button type="submit" class="btn btn-primary">Search</button>
						</form>
					</div>

					<c:if test="${not empty foods}">
						<div>
							<h2 class="page-header">Food List</h2>
							<div class="list-group" style="height: 250px; overflow-y: scroll">
								<c:forEach items="${foods}" var="prod" varStatus="status">
									<c:if test="${prod.defaultServingSize > 0}">
										<a id="list${status.count}"
											onclick="selectFood(${status.count})" class="list-group-item">${prod.name}(${prod.brand})</a>
										<p id="data${status.count}" class="sr-only">
											${prod.name}@${prod.brand}@${prod.defaultUnit.name}@${prod.foodId}@${prod.calories}@${prod.defaultServingSize}</p>
									</c:if>
								</c:forEach>
							</div>
						</div>
						<div>
							<h2 class="page-header">Food Intake</h2>
							<i id="slectedFoodName"></i>
							<form:form class="form-inline" role="form"
								action="foodCalculateCalories" commandName="foodLog">
								<form:input type="text" class="form-control" path="quantity" />
								<form:input id="selectedFoodId" class="form-control sr-only"
									path="foodId" />
								<form:input id="selectedFoodName" class="form-control sr-only"
									path="foodName" />
								<form:input id="selectedFoodCalories"
									class="form-control sr-only" path="caloriesPerUnit" />
								<form:input id="selectedFoodDefaultServingSize"
									class="form-control sr-only" path="defaultServingSize" />
								<select class="form-control" id="sel1">
									<option id="unit">btrs</option>
								</select>
								<input type="submit" class="btn btn-primary" value="Submit">
							</form:form>
							<br>
						</div>
					</c:if>
				</div>

	</div> <!-- /container -->
	
	
	
	<div id="footerwrap">
      	<footer class="clearfix"></footer>
      	<div class="container">
      		<div class="row">
      			<div class="col-sm-12 col-lg-12">
      			
      			<p>GetFit Home - improving lives - Copyright 2014</p>
      			</div>

      		</div><!-- /row -->
      	</div><!-- /container -->		
	</div><!-- /footerwrap -->

	<!-- canvas JS for Chart -->
	<script src="<c:url value="/resources/js/canvasjs.min.js" />"></script>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>    
	<script type="text/javascript" src="<c:url value="/resources/assets/js/lineandbars.js"/>"></script>
    
	<script type="text/javascript" src="<c:url value="/resources/assets/js/dash-charts.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/assets/js/gauge.js"/>"></script>
	
	<!-- NOTY JAVASCRIPT -->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/jquery.noty.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/top.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topLeft.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topRight.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topCenter.js"></script>
	
	
	<!-- You can add more layouts if you want -->
	<script type="text/javascript" src="<c:url value="/resources/assets/js/noty/themes/default.js"/>"></script>
    <!-- <script type="text/javascript" src="assets/js/dash-noty.js"></script> This is a Noty bubble when you init the theme-->
	<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
	<script src="<c:url value="/resources/assets/js/jquery.flexslider.js"/>" type="text/javascript"></script>

    <script type="text/javascript" src="<c:url value="/resources/assets/js/admin.js"/>"></script>
  
</body>
<script>
var preCount = 0;
function selectFood(count) {
	$("#list" + preCount).attr("class", "list-group-item");
	preCount = count;
	$("#list" + count).attr("class", "list-group-item active");
	var res = $("#" + "data" + count).text().split("@");
	$("#slectedFoodName").text(res[0] + "(" + res[1] + ")");
	$("#unit").text(res[2]);

	/* Set values sent to server */
	$("#selectedFoodDefaultServingSize").val(res[5]);
	$("#selectedFoodCalories").val(res[4]);
	$("#selectedFoodId").val(res[3]);
	$("#selectedFoodName").val(res[0]);
}
</script>
</html>



