<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>

<head>
	<title>Exercise History</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">   
	
	<!-- Le styles -->

    <link href="<c:url value="/resources/assets/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/main.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/font-style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/flexslider.css"/>" rel="stylesheet">
    
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>

    <style type="text/css">
      body {
        padding-top: 60px;
      }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

  	<!-- Google Fonts call. Font Used Open Sans & Raleway -->
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
  	<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

</head>
<body>
  <!-- NAVIGATION MENU -->

    <div class="navbar-nav navbar-inverse navbar-fixed-top">
        <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="validateLogin"><img src="<c:url value="/resources/assets/img/logo30.png"/>" alt=""> GetFit</a>
        </div> 
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="index.html"><i class="icon-home icon-white"></i> Home</a></li>
              <li><a href="bloodHome"><i class="icon-th icon-white"></i> Blood & Medication</a></li>
              <li class="active"><a href="exerciseHome"><i class="icon-user icon-white"></i> Exercise</a></li>
              <li><a href="foodLogTrack"><i class="icon-user icon-white"></i> Diet</a></li>
              <li><a href="user.html"><i class="icon-user icon-white"></i> Stress</a></li>
              <li><a href="sleep"><i class="icon-user icon-white"></i> Sleep</a></li>

              <li><a href="login.html"><i class="icon-lock icon-white"></i> Logout</a></li>

            </ul>
          </div><!--/.nav-collapse -->
        </div>
    </div>
	
    <div class="container-fluid">
      <div class="row">
      	
      	<!-- Sidebar -->
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="exerciseHome">Overview</a></li>
            <li class="active"><a href="#">History</a></li>
          </ul>
        </div>
        
        <!-- Body -->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<h3>Past Exercise</h3>
        	<p>
        		<select>
					<option value="Calories">Calories Burned</option>
					<option value="Distance">Distance Traveled</option>
					<option value="Steps">Steps Taken</option>
					<option value="VeryActiveMinutes">Very Active Minutes</option>
				</select>
				<select>
					<option value="Week">Week</option>
					<option value="Month">Month</option>
					<option value="Year">Year</option>
				</select>
				<div id="chartContainer" style="height: 300px; width: 100%;"></div>
        	</p>
        </div>
      </div>
    </div>
    

    
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/canvasjs.min.js"  />"></script>
<script type="text/javascript">
 	window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer", {

	//title:{text: "Fruits sold in First Quarter"},
	data: [//array of dataSeries              
		{ //dataSeries object
			/*** Change type "column" to "bar", "area", "line" or "pie"***/
			type: "line",
			dataPoints: [
			{ label: "22/10/14", y: 1774 },
			{ label: "orange", y: 29 },
			{ label: "apple", y: 40 },                                    
			{ label: "mango", y: 34 },
			{ label: "grape", y: 24 }
			]
		}
	]
	});
   	chart.render();
   	}
</script>
</html>
