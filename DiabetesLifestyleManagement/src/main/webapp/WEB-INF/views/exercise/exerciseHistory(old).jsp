<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>

<head>
	<title>Exercise History</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">   
	<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/css/home.css" />" rel="stylesheet">
</head>
<body>
  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Diabetes Management</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-left">
            <li><a href="introduction" >Home</a></li>
            <li class = "active"><a href="exerciseHome">Exercise</a></li>
            <li><a href="foodHome">Food</a></li>
          </ul>
        </div>
      </div>
    </div>
	
    <div class="container-fluid">
      <div class="row">
      	
      	<!-- Sidebar -->
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="exerciseHome">Overview</a></li>
            <li class="active"><a href="#">History</a></li>
          </ul>
        </div>
        
        <!-- Body -->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<h3>Welcome to the Exercise Component</h3>
        	<h5>Past Exercise</h5>
        	<p>
        		<select>
					<option value="Calories">Calories Burned</option>
					<option value="Distance">Distance Traveled</option>
					<option value="Steps">Steps Taken</option>
					<option value="VeryActiveMinutes">Very Active Minutes</option>
				</select>
				<select>
					<option value="Week">Week</option>
					<option value="Month">Month</option>
					<option value="Year">Year</option>
				</select>
				<div id="chartContainer" style="height: 300px; width: 100%;"></div>
        	</p>
        </div>
      </div>
    </div>
    

    
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/canvasjs.min.js"  />"></script>
<script type="text/javascript">
 	window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer", {

	//title:{text: "Fruits sold in First Quarter"},
	data: [//array of dataSeries              
		{ //dataSeries object
			/*** Change type "column" to "bar", "area", "line" or "pie"***/
			type: "line",
			dataPoints: [
			{ label: "22/10/14", y: 1774 },
			{ label: "orange", y: 29 },
			{ label: "apple", y: 40 },                                    
			{ label: "mango", y: 34 },
			{ label: "grape", y: 24 }
			]
		}
	]
	});
   	chart.render();
   	}
</script>
</html>
