<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html><head>
    <meta charset="utf-8">
    <title>GetFit | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Carlos Alvarez - Alvarez.is">

    <!-- Le styles -->

    <link href="<c:url value="/resources/assets/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/main.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/font-style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/flexslider.css"/>" rel="stylesheet">
    
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>

    <style type="text/css">
      body {
        padding-top: 60px;
      }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

  	<!-- Google Fonts call. Font Used Open Sans & Raleway -->
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
  	<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

<script type="text/javascript">
$(document).ready(function () {

    $("#btn-blog-next").click(function () {
      $('#blogCarousel').carousel('next')
    });
     $("#btn-blog-prev").click(function () {
      $('#blogCarousel').carousel('prev')
    });

     $("#btn-client-next").click(function () {
      $('#clientCarousel').carousel('next')
    });
     $("#btn-client-prev").click(function () {
      $('#clientCarousel').carousel('prev')
    });
    
});

 $(window).load(function(){

    $('.flexslider').flexslider({
        animation: "slide",
        slideshow: true,
        start: function(slider){
          $('body').removeClass('loading');
        }
    });  
});

</script>


    
  </head>
  <body>
  
  	<!-- NAVIGATION MENU -->

    <div class="navbar-nav navbar-inverse navbar-fixed-top">
        <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="introduction"><img src="<c:url value="/resources/assets/img/logo30.png"/>" alt=""> GetFit</a>
        </div> 
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="introduction"><i class="icon-home icon-white"></i> Home</a></li>
              <li><a href="login.html"><i class="icon-lock icon-white"></i> Login with Fitbit</a></li>

            </ul>
          </div><!--/.nav-collapse -->
        </div>
    </div>
   <div class="container">
   
	<h3><center>
	Welcome and GetFit!
	</center></h3>
	<div class="col-sm-12 col-lg-12">
	<p><font color="white">
	 You just landed on a leading lifestyle management platform which will help you improve the quality of your life.
	 GetFit utilises a pool of data that is recorded by the physical activity tracking products of fitbit�. Data such as steps taken,
	 distance travelled, calories burned, active minutes, hours slept and quality of sleep are captured by the application.
	 Then, further analyses are performed to produce enhanced visualisations and custom recommendations to fit the lifestyle of Diabetes II sufferers.
	</font></p>
</div>
	</div> <!-- /container -->
	<div id="footerwrap">
      	<footer class="clearfix"></footer>
      	<div class="container">
      		<div class="row">
      			<div class="col-sm-12 col-lg-12">
      			<p><img src="<c:url value="/resources/assets/img/logo.png"/>" alt=""></p>
      			<p>GetFit Home - improving lives - Copyright 2014</p>
      			</div>

      		</div><!-- /row -->
      	</div><!-- /container -->		
	</div><!-- /footerwrap -->


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>    
	<script type="text/javascript" src="<c:url value="/resources/assets/js/lineandbars.js"/>"></script>
    
	<script type="text/javascript" src="<c:url value="/resources/assets/js/dash-charts.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/assets/js/gauge.js"/>"></script>
	
	<!-- NOTY JAVASCRIPT -->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/jquery.noty.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/top.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topLeft.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topRight.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topCenter.js"></script>
	
	
	<!-- You can add more layouts if you want -->
	<script type="text/javascript" src="<c:url value="/resources/assets/js/noty/themes/default.js"/>"></script>
    <!-- <script type="text/javascript" src="assets/js/dash-noty.js"></script> This is a Noty bubble when you init the theme-->
	<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
	<script src="<c:url value="/resources/assets/js/jquery.flexslider.js"/>" type="text/javascript"></script>

    <script type="text/javascript" src="<c:url value="/resources/assets/js/admin.js"/>"></script>
  
</body></html>