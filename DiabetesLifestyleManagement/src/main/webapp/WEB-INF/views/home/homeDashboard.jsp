<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html><head>
    <meta charset="utf-8">
    <title>GetFit | DashBoard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Carlos Alvarez - Alvarez.is">

    <!-- Le styles -->

    <link href="<c:url value="/resources/assets/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/main.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/font-style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/flexslider.css"/>" rel="stylesheet">
    
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>

    <style type="text/css">
      body {
        padding-top: 60px;
      }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

  	<!-- Google Fonts call. Font Used Open Sans & Raleway -->
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
  	<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

<script type="text/javascript">
$(document).ready(function () {

    $("#btn-blog-next").click(function () {
      $('#blogCarousel').carousel('next')
    });
     $("#btn-blog-prev").click(function () {
      $('#blogCarousel').carousel('prev')
    });

     $("#btn-client-next").click(function () {
      $('#clientCarousel').carousel('next')
    });
     $("#btn-client-prev").click(function () {
      $('#clientCarousel').carousel('prev')
    });
    
});

 $(window).load(function(){

    $('.flexslider').flexslider({
        animation: "slide",
        slideshow: true,
        start: function(slider){
          $('body').removeClass('loading');
        }
    });  
});

</script>


    
  </head>
  <body>
  
  	<!-- NAVIGATION MENU -->

    <div class="navbar-nav navbar-inverse navbar-fixed-top">
        <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="validateLogin"><img src="<c:url value="/resources/assets/img/logo30.png"/>" alt=""> GetFit</a>
        </div> 
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="index.html"><i class="icon-home icon-white"></i> Home</a></li>
              <li><a href="bloodHome"><i class="icon-th icon-white"></i> Blood Sugar</a></li>
              <li><a href="exerciseHome"><i class="icon-user icon-white"></i> Exercise</a></li>
              <li><a href="foodLogTrack"><i class="icon-user icon-white"></i> Diet</a></li>
              <li><a href="sleep"><i class="icon-user icon-white"></i> Sleep</a></li>
              <li><a href="#"><i class="icon-user icon-white"></i> Stress</a></li>
              <li><a href="login"><i class="icon-lock icon-white"></i> Logout</a></li>

            </ul>
          </div><!--/.nav-collapse -->
        </div>
    </div>

    <div class="container">

	  <!-- FIRST ROW OF BLOCKS -->     
      <div class="row">

      <!-- USER PROFILE BLOCK -->
        <div class="col-sm-3 col-lg-3">
      		<div class="dash-unit">
	      		<dtitle>User Profile</dtitle>
	      		<hr>
				<div class="thumbnail">
					<img src="<c:url value="${avatar}"/>">
				</div><!-- /thumbnail -->
				<h1>Hi ${name}!</h1>
				<h3>Height: ${height} Cm</h3>
				<h3>Weight: ${weight} Kg</h3>
				<h3>D.O.B : ${birth}</h3>
				<br>
					<div class="info-user">
						
					</div>
				</div>
        </div>

      <!-- DONUT CHART BLOCK -->
        <div class="col-sm-3 col-lg-3">
      		<div class="dash-unit">
		  		<dtitle>Daily Activity Performance</dtitle>
		  		<hr>
	        	<div id="load"></div>
	        	<h2>${activity}%</h2><h3>of the ${StepsTarget} steps target</h3>
			</div>
        </div>

      <!-- DONUT CHART BLOCK -->
        <div class="col-sm-3 col-lg-3">
      		<div class="dash-unit">
		  		<dtitle>Daily Dietary Intake</dtitle>
		  		<hr>
	        	<div id="space"></div>
	        	<h2>${calories}%</h2>
	        	<h3>of the ${CaloriesTarget} cal. target</h3>
			</div>
        </div>
        
        <div class="col-sm-3 col-lg-3">

      <!-- LOCAL TIME BLOCK -->
      		<div class="half-unit">
	      		<dtitle>Local Time</dtitle>
	      		<hr>
		      		<div class="clockcenter">
			      		<digiclock>12:45:25</digiclock>
		      		</div>
			</div>

			<div class="half-unit">
	      		<dtitle>Compared to yesterday</dtitle>
	      		<hr>
	      		<div class="cont">
					<p><img src="<c:url value="/resources/assets/img/up.png"/>" alt=""> <bold>${statement}</bold> |   ${comparison}% difference in performance</p>
					
				</div>
			</div>

        </div>
      </div><!-- /row -->
      
      
	  <!-- SECOND ROW OF BLOCKS -->     
      <div class="row">
        <div class="col-sm-3 col-lg-3">
       <!-- MAIL BLOCK -->
      		<div class="dash-unit">
      		<dtitle>Older Challenges</dtitle>
      		<hr>
      		<div class="framemail">
    			<div class="window">
			        <ul>
			            <li>
			                
			                <p class="message">1. ${challenges0}</p>
			               
			            </li>
			            <li>
			          
			                <p class="message">2. ${challenges1}</p>
			            </li>
			            <li>
			              
			                <p class="message">3. ${challenges2}</p>
			            </li>
			        </ul>
    			</div>
			</div>
		</div><!-- /dash-unit -->
    </div><!-- /span3 -->

	  <!-- GRAPH CHART - lineandbars.js file -->     
        <div class="col-sm-3 col-lg-3">
      		<div class="dash-unit">
      		<dtitle>Sleep Patterns</dtitle>
      		<hr>
			    <div class="section-graph">
			      <div id="importantchart"></div>
			      <br>
			      <div class="graph-info">
			        <i class="graph-arrow"></i>
			        <span class="graph-info-big">8 hrs</span>
			        <span class="graph-info-small">3.71% better than average</span>
			      </div>
			    </div>
			</div>
        </div>

	  <!-- LAST MONTH REVENUE -->     
        <div class="col-sm-3 col-lg-3">
      		<div class="dash-unit">
	      		<dtitle>Stress Level</dtitle>
	      		<hr>
	      		<div class="cont">
					<p>you seem to have low stress level | <ok>well done</ok></p>
					<br>
					
				</div>

			</div>
        </div>
        
	  <!-- 30 DAYS STATS - CAROUSEL FLEXSLIDER -->     
        <div class="col-sm-3 col-lg-3">
      		<div class="dash-unit">
	      		<dtitle>Today's Challenge</dtitle>
	      		<hr>
	      		<br>
	      		<br>
				<div class="cont">
					<p><a href="http://maps.google.com/maps?q=${place}" target="_blank">${challenge}</a></p>
				</div>
			</div>
        </div>
      </div><!-- /row -->

	</div> <!-- /container -->
	<div id="footerwrap">
      	<footer class="clearfix"></footer>
      	<div class="container">
      		<div class="row">
      			<div class="col-sm-12 col-lg-12">
      			<p><img src="/resources/assets/img/logo.png" alt=""></p>
      			<p>Blocks Dashboard Theme - Crafted With Love - Copyright 2013</p>
      			</div>

      		</div><!-- /row -->
      	</div><!-- /container -->		
	</div><!-- /footerwrap -->


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>    
	<script type="text/javascript" src="<c:url value="/resources/assets/js/lineandbars.js"/>"></script>
    
	<script type="text/javascript" src="<c:url value="/resources/assets/js/dash-charts.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/assets/js/gauge.js"/>"></script>
	
	<!-- NOTY JAVASCRIPT -->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/jquery.noty.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/top.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topLeft.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topRight.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topCenter.js"></script>
	
	
	<!-- You can add more layouts if you want -->
	<script type="text/javascript" src="<c:url value="/resources/assets/js/noty/themes/default.js"/>"></script>
    <!-- <script type="text/javascript" src="assets/js/dash-noty.js"></script> This is a Noty bubble when you init the theme-->
	<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
	<script src="<c:url value="/resources/assets/js/jquery.flexslider.js"/>" type="text/javascript"></script>

    <script type="text/javascript" src="<c:url value="/resources/assets/js/admin.js"/>"></script>
   
   <script type="text/javascript">
   $(document).ready(function() 
		   {
	   var activity = ${activity};
		info = new Highcharts.Chart({
			chart: {
				renderTo: 'load',
				margin: [0, 0, 0, 0],
				backgroundColor: null,
               plotBackgroundColor: 'none',
							
			},
			
			title: {
				text: null
			},
			
			credits: {
	            enabled: false
	        },

			tooltip: {
				formatter: function() { 
					return this.point.name +': '+ this.y +' %';
						
				} 	
			},
		    series: [
				{
				borderWidth: 2,
				borderColor: '#F1F3EB',
				shadow: false,	
				type: 'pie',
				name: '',
				innerSize: '65%',
				data: [
					{ name: 'steps percentage', y: activity+1, color: '#b2c831' },
					{ name: 'inactivity', y: 100-activity, color: '##fa1d2d' }
				],
				dataLabels: {
					enabled: false,
					color: '#000000',
					connectorColor: '#000000'
				}
			}]
		});
		
	});

/*** second Chart in Dashboard page ***/

	$(window).load(function() {
		var calories = ${calories};

		info = new Highcharts.Chart({
			chart: {
				renderTo: 'space',
				margin: [0, 0, 0, 0],
				backgroundColor: null,
               plotBackgroundColor: 'none',
							
			},
			
			title: {
				text: null
			},
			
			credits: {
	            enabled: false
	        },

			tooltip: {
				formatter: function() { 
					return this.point.name +': '+ this.y +' %';
						
				} 	
			},
		    series: [
				{
				borderWidth: 2,
				borderColor: '#F1F3EB',
				shadow: false,	
				type: 'pie',
				name: '',
				innerSize: '65%',
				data: [
					{ name: 'Out', y: calories, color: '#fa1d2d' },
					{ name: 'In', y: 100-calories, color: '#3d3d3d' }
				],
				dataLabels: {
					enabled: false,
					color: '#000000',
					connectorColor: '#000000'
				}
			}]
		});
		
	});

   </script>
</body></html>