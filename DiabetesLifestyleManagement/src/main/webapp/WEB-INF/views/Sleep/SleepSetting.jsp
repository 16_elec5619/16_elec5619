<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>

<head>
	<title>GetFit - Sleep</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">   
	<!-- Le styles -->

    <link href="<c:url value="/resources/assets/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/main.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/font-style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/flexslider.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/assets/css/table.css"/>" rel="stylesheet">
    
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>

    <style type="text/css">
      body {
        padding-top: 60px;
      }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

  	<!-- Google Fonts call. Font Used Open Sans & Raleway -->
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
  	<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
	
</head>
<body>

	<!-- NAVIGATION MENU -->

    <div class="navbar-nav navbar-inverse navbar-fixed-top">
        <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="validateLogin"><img src="<c:url value="/resources/assets/img/logo30.png"/>" alt=""> GetFit</a>
        </div> 
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="../dashboard"><i class="icon-home icon-white"></i> Home</a></li>
              <li><a href="../bloodHome"><i class="icon-th icon-white"></i> Blood & Medication</a></li>
              <li><a href="../exerciseHome"><i class="icon-user icon-white"></i> Exercise</a></li>
              <li><a href="../foodLogTrack"><i class="icon-user icon-white"></i> Diet</a></li>
              <li><a href="../user.html"><i class="icon-user icon-white"></i> Stress</a></li>
              <li clas="active"><a href="../sleep"><i class="icon-user icon-white"></i> Sleep</a></li>

              <li><a href="login.html"><i class="icon-lock icon-white"></i> Logout</a></li>

            </ul>
          </div><!--/.nav-collapse -->
        </div>
    </div>
	
    <div style = "background-color: white;" class="container">
      <div class="row">
      
        <!-- side bar -->
		<div class="col-sm-3 col-md-2 sidebar">
			<ul class="nav nav-sidebar">
				<li ><a href="sleep">Home</a></li>
				<li class="active"><a href="settings">Settings</a></li>
			</ul>
		</div>
        <!-- Body -->
        <div class="col-sm-9 col-md-10  main">
        	<h1 class="page-header">Sleep Monitoring Settings</h1>
			   			
				<form:form method="POST" action="../sleep/settings" commandName="settingsForm" modelAttribute = "sleepGoal">
			
					        <form:label path="goalHours">Goal hours per night</form:label></td>
					        <form:input path="goalHours" /></td>
				
					        <td><form:hidden path="dateCreated" value= "${date}"/></td>
		
					<input type="submit" value="Submit"/>
				</form:form>
				
				<h2>Previous Sleep Goals:</h2>
				<table class="display">
					<tr>
						<td> Goal Hours </td>
						<td> Date Created </td>
					</tr>
				<c:if test="${not empty goalLists }">
					
						<c:forEach var="data" items="${goalLists}">
							<tr>
								<td> ${data.getGoalHours()} </td>
								<td> ${data.getDateCreated()} </td>
							</tr>
						</c:forEach>
				</c:if>
				</table>
					
   			
   			
        </div>
      </div>
    </div>
    
	<!-- canvas JS for Chart -->
	<script src="<c:url value="/resources/js/canvasjs.min.js" />"></script>
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>    
	<script type="text/javascript" src="<c:url value="/resources/assets/js/lineandbars.js"/>"></script>
    
	<script type="text/javascript" src="<c:url value="/resources/assets/js/dash-charts.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/assets/js/gauge.js"/>"></script>
	
	<!-- NOTY JAVASCRIPT -->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/jquery.noty.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/top.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topLeft.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topRight.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.2.2/layouts/topCenter.js"></script>
	
	
	<!-- You can add more layouts if you want -->
	<script type="text/javascript" src="<c:url value="/resources/assets/js/noty/themes/default.js"/>"></script>
    <!-- <script type="text/javascript" src="assets/js/dash-noty.js"></script> This is a Noty bubble when you init the theme-->
	<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
	<script src="<c:url value="/resources/assets/js/jquery.flexslider.js"/>" type="text/javascript"></script>

    <script type="text/javascript" src="<c:url value="/resources/assets/js/admin.js"/>"></script>
    
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>


</html>
