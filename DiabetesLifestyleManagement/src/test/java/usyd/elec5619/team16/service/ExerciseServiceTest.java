package usyd.elec5619.team16.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import usyd.elec5619.team16.model.Exercise;
import usyd.elec5619.team16.model.User;
import junit.framework.TestCase;

public class ExerciseServiceTest extends TestCase {
	@Autowired
	private ExerciseService exerciseService;
	
	@Autowired
	private UserService userService;
	
	public void testGetLastUpdate() {
		User user = new User();
		user.setName("UnitTest");
		userService.addUser(user);
		
		assertNull(exerciseService.getLastUpdate(user));
		
		Exercise exercise = new Exercise();
		exercise.setUser(user);
		Date testDate = new Date();
		exercise.setDate(testDate);		
		exerciseService.addExercise(exercise);
		
		assertEquals(testDate,exerciseService.getLastUpdate(user));
	}

}
