package usyd.elec5619.team16.model;

import java.util.Date;

import junit.framework.TestCase;

public class ExerciseTest extends TestCase {
	private Exercise exercise;
	
	protected void setUp() throws Exception {
		exercise = new Exercise();
	}
	
	public void testSetAndGetDate() {
		Date testDate = new Date();
		assertNull(exercise.getDate());
		exercise.setDate(testDate);
		assertEquals(testDate, exercise.getDate());
	}
	public void testSetAndGetWeight() {
		double testWeight = 70.1;
		assertNull(exercise.getWeight());
		exercise.setWeight(testWeight);
		assertEquals(testWeight, exercise.getWeight());
	}
	public void testSetAndGetCaloriesBurned() {
		Integer testCalories = 1703;
		assertNull(exercise.getCaloriesBurned());
		exercise.setCaloriesBurned(testCalories);
		assertEquals(testCalories, exercise.getCaloriesBurned());
	}
	public void testSetAndGetCalorieTarget() {
		Integer testCalories = 2000;
		assertNull(exercise.getCalorieTarget());
		exercise.setCalorieTarget(testCalories);
		assertEquals(testCalories, exercise.getCalorieTarget());
	}
}