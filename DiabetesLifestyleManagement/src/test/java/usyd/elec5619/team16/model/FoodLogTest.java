package usyd.elec5619.team16.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
public class FoodLogTest {
	FoodLog foodLog=null;
	
	@Before
	public void init(){
		foodLog = new FoodLog();
	}
	
	@Test
	public void testSetAndGetFoodId() {		
		Long testId =  123132L;
		foodLog.setFoodId(testId);
		Long testId2 =  foodLog.getFoodId();
		assertEquals(testId, testId2);
	}
	
	@Test
	public void testSetAndGetFoodName() {
		String testName =  "Tomato";
		foodLog.setFoodName(testName);
		assertEquals(testName, foodLog.getFoodName());
	}
	
	@Test
	public void testSetAndGetCalories() {
		Double testCalories =  123.3D;
		foodLog.setCalories(testCalories);
		Double testCalories2 = foodLog.getCalories();
		assertEquals(testCalories, testCalories2);
	}
	
	@Test
	public void testSetAndGetQuantity() {
		Double testQuantity =  12D;
		foodLog.setQuantity(testQuantity);
		Double testQuantity2 = foodLog.getQuantity();
		assertEquals(testQuantity, testQuantity2);
	}
	
	@Test
	public void testSetAndGetCaloriesPerUnit() {
		Double testCaloriesPerUnit =  121D;
		foodLog.setCaloriesPerUnit(testCaloriesPerUnit);
		Double testCaloriesPerUnit2 = foodLog.getCaloriesPerUnit();
		assertEquals(testCaloriesPerUnit, testCaloriesPerUnit2);
	}
	
	@Test
	public void testSetAndGetDefaultServingSize() {
		Double testCaloriesPerUnit =  121D;
		foodLog.setDefaultServingSize(testCaloriesPerUnit);
		Double testCaloriesPerUnit2 = foodLog.getDefaultServingSize();
		assertEquals(testCaloriesPerUnit, testCaloriesPerUnit2);
	}
	
	
	
}
