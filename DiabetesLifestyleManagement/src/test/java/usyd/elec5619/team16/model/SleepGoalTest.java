package usyd.elec5619.team16.model;

import java.util.Date;

import junit.framework.TestCase;

public class SleepGoalTest extends TestCase {
	private SleepGoal goal;
	
	protected void setUp() throws Exception {
		goal = new SleepGoal();
	}
	
	public void testSetAndGetDate(){
		Date testDate = new Date();
		assertNull(goal.getDateCreated());
		goal.setDateCreated(testDate);
		assertEquals(goal.getDateCreated(),testDate);
	}
	
	public void testSetAndGetGoalHours(){
		int testGoal = 5;
		goal.setGoalHours(testGoal);
		assertEquals(goal.getGoalHours(),testGoal);
	}
}
