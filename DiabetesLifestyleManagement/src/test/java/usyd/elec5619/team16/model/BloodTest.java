package usyd.elec5619.team16.model;

import java.util.Date;

import junit.framework.TestCase;

public class BloodTest extends TestCase {
	private BloodSugar sugar;
	
	protected void setUp() throws Exception {
		sugar = new BloodSugar();
	}
	
	public void testSetAndGetDate() {
		Date testDate = new Date();
		assertNull(sugar.getDate());
		sugar.setDate(testDate);
		assertEquals(testDate, sugar.getDate());
	}
	public void testSetAndGetSugarLevel() {
		Double testLevel = 2.3;
		assertNull(sugar.getSugarLevel());
		sugar.setSugarLevel(testLevel);
		assertEquals(testLevel, sugar.getSugarLevel());
	}
}