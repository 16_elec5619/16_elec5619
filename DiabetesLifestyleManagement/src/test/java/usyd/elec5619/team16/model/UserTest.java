package usyd.elec5619.team16.model;

import junit.framework.TestCase;

public class UserTest extends TestCase {
	private User user;
	
	protected void setUp() throws Exception {
		user = new User();
	}
	
	public void testSetAndGetEmail() {
		String testEmail = "test@email.com";
		assertNull(user.getEmail());
		user.setEmail(testEmail);
		assertEquals(testEmail, user.getEmail());
	}
	public void testSetAndGetName() {
		String testName = "testing";
		assertNull(user.getName());
		user.setName(testName);
		assertEquals(testName, user.getName());
	}
	public void testSetAndGetGender() {
		String testGender = "M";
		assertNull(user.getGender());
		user.setGender(testGender);
		assertEquals(testGender, user.getGender());
	}
	public void testSetAndGetPW() {
		String testPW = "tesing123";
		assertNull(user.getPW());
		user.setPW(testPW);
		assertEquals(testPW, user.getPW());
	}
	
	public void testSetAndGetAvatar() {
		String testAvatar = "somelongurl";
		assertNull(user.getAvatar());
		user.setAvatar(testAvatar);
		assertEquals(testAvatar, user.getAvatar());
	}
	
}