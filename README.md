# README #

This repository has a fitbit-client application- A Java Web Application that retrieves private data stored by FitBit. It provides an example of how to authenticate using OAuth, get Data and do some analysis and visualisations.

The development of this project is a part of ELEC5619 OO DEVELOPMENT FRAMEWORKS, THE UNIVERSITY OF SYDNEY, AUSTRALIA.




You can clone or download the repo as a zip.
Feel Free to contact any of the team members if you have any questions.